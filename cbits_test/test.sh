#!/bin/sh

set -eu

$CC call.c -o call.wasm

wasmtime run call.wasm

$CC closure.c -o closure.wasm

wasmtime run closure.wasm
