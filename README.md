# `libffi-wasm`

Limited port of `libffi` to pure `wasm32`.

Note that this repo's issue tracker is closed; please report issues in
ghc with the `wasm` tag instead.

## Limitations compared to vanilla `libffi`

- Function argument count is capped to `4`
- For each function type, closure count is capped to `16`. Both
  constants can be tuned, and exceptions can be made for specific
  function types
- Closure API is modified, closure alloc/prep is done in a single call
- No support for structures or variadic functions yet

## How to use

This is a Haskell cabal project, simply run the `libffi-wasm`
executable. It'll generate additional C sources in `cbits`. Then
include stuff in `cbits` for your own usage.

## How?

See this [blog
post](https://www.tweag.io/blog/2022-03-17-libffi-wasm32) for details.
